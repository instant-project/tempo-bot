<?php

declare(strict_types=1);

namespace TempoBot\Config;

use InvalidArgumentException;
use JsonException;
use JsonSerializable;
use stdClass;
use TempoBot\Config\Distribution\Bucket;
use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidException;
use TempoBot\Config\Exception\InputInvalidJSONException;

abstract class BaseConfig implements JsonSerializable
{
    /** @var string */
    protected $tempoAccountId;

    /** @var string */
    protected $tempoAccessToken;

    /** @var Bucket[] */
    protected $distribution = [];

    protected function __construct(string $config)
    {
        $this->guardAgainstInvalidJson($config);
        $decodedInput = $this->decodeJson($config);

        $this->setTempoAccountId($decodedInput->tempoAccountId ?? '');
        $this->setTempoAccessToken($decodedInput->tempoAccessToken ?? '');

        $distribution = [];
        foreach ($decodedInput->distribution ?? [] as $bucket) {
            $distribution[] = Bucket::buildFromDecodedJson($bucket);
        }

        $this->setDistribution($distribution);
    }

    /**
     * @param string $config
     *
     * @throws InputInvalidJSONException
     * @throws InputEmptyException
     */
    protected function guardAgainstInvalidJson(string $config)
    {
        if (strlen(trim($config)) <= 0) {
            throw new InputEmptyException();
        }

        try {
            json_decode($config, false, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new InputInvalidJSONException();
        }
    }

    protected function decodeJson(string $config): stdClass
    {
        return json_decode($config, false, 512);
    }

    public function asPrettyJson(): string
    {
        return json_encode($this, JSON_PRETTY_PRINT);
    }

    public function jsonSerialize()
    {
        $output = new stdClass();

        $output->tempoAccountId = $this->getTempoAccountId();
        $output->tempoAccessToken = $this->getTempoAccessToken();
        $output->distribution = [];
        foreach ($this->getDistribution() as $bucket) {
            $output->distribution[] = $bucket->jsonSerialize();
        }

        return $output;
    }

    public function getTempoAccountId(): string
    {
        return $this->tempoAccountId;
    }

    public function setTempoAccountId(string $tempoAccountId): void
    {
        if (strlen(trim($tempoAccountId)) <= 0) {
            throw new InvalidArgumentException('Tempo account id was empty');
        }

        $this->tempoAccountId = $tempoAccountId;
    }

    public function getTempoAccessToken(): string
    {
        return $this->tempoAccessToken;
    }

    public function setTempoAccessToken(string $tempoAccessToken): void
    {
        if (strlen(trim($tempoAccessToken)) <= 0) {
            throw new InvalidArgumentException('Tempo access token was empty');
        }

        $this->tempoAccessToken = $tempoAccessToken;
    }

    public function getDistribution(): array
    {
        return $this->distribution;
    }

    public function setDistribution(array $buckets)
    {
        $this->distribution = [];
        foreach ($buckets as $bucket) {
            $this->setDistributionBucket($bucket);
        }
        if (count($this->getDistribution()) <= 0) {
            throw new InputInvalidException('Distribution is not allowed to be empty');
        }
    }

    private function setDistributionBucket($bucket)
    {
        if (!$bucket instanceof Bucket) {
            throw new InputInvalidException('Distribution may only contain buckets');
        }
        $this->distribution[] = $bucket;
    }
}
