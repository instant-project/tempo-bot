<?php

declare(strict_types=1);

namespace TempoBot\Config;

use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidJSONException;

/**
 * @codeCoverageIgnore
 * @package TempoBot\Config
 */
class ConfigFactory
{
    /**
     * @param string $input
     *
     * @return Config
     * @throws InputInvalidJSONException
     * @throws InputEmptyException
     */
    public function build(string $input): Config
    {
        return Config::fromJsonString($input);
    }

    public function buildEmpty(): EmptyConfig
    {
        return new EmptyConfig();
    }
}