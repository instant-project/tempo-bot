<?php

declare(strict_types=1);

namespace TempoBot\Config\Repository;

use Predis\Client;
use TempoBot\Config\Config;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidException;
use TempoBot\Config\Exception\InputInvalidJSONException;

class RedisConfigRepository implements ConfigRepositoryInterface
{
    /** @var Client */
    private $redisClient;

    /** @var ConfigFactory */
    private $configFactory;

    /** @var string */
    private const KEY_PREFIX = 'tempo-bot:repositories:config:%s';

    public function __construct(Client $redisClient, ConfigFactory $configFactory)
    {
        $this->redisClient = $redisClient;
        $this->configFactory = $configFactory;
    }

    public function getCurrentForUser(string $accountId): ?Config
    {
        $key = $this->keyFor($accountId);
        $value = $this->redisClient->get($key);

        try {
            return $this->configFactory->build((string) $value);
        } catch (InputInvalidException $e) {
            return null;
        } catch (InputInvalidJSONException $e) {
            return null;
        } catch (InputEmptyException $e) {
            return null;
        }
    }

    public function createOrUpdate(string $accountId, Config $config): void
    {
        $key = sprintf(self::KEY_PREFIX, $accountId);
        $this->redisClient->set($key, $config->asPrettyJson());
    }

    /**
     * @inheritDoc
     */
    public function retrieveAll(): array
    {
        $output = $configKeys = [];

        $cursor = 0;
        do {
            $results = $this->redisClient->scan($cursor, [
                'match' => $this->keyFor('*')
            ]);

            $configKeys = array_merge($configKeys, $results[1]);
            $cursor = (int) $results[0];

        } while ($cursor != 0);

        foreach ($configKeys as $configKey) {
            $accountId = str_ireplace($this->keyFor(''), '', $configKey);
            $output[$accountId] = $this->getCurrentForUser($accountId);
        }

        return array_filter($output);
    }

    protected function keyFor($accountId): string
    {
        return sprintf(self::KEY_PREFIX, $accountId);
    }
}
