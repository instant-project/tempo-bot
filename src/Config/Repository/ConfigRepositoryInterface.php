<?php

declare(strict_types=1);

namespace TempoBot\Config\Repository;

use TempoBot\Config\Config;

interface ConfigRepositoryInterface
{
    public function getCurrentForUser(string $accountId): ?Config;

    public function createOrUpdate(string $accountId, Config $config): void;

    /**
     * @return Config[]
     */
    public function retrieveAll(): array;
}
