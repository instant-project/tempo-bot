<?php

declare(strict_types=1);

namespace TempoBot\Config\Repository;

use TempoBot\Config\Config;

class ArrayConfigRepository implements ConfigRepositoryInterface
{
    /** @var array */
    private $storage;

    public function __construct(array $storage = [])
    {
        $this->storage = $storage;
    }

    public function createOrUpdate(string $accountId, Config $config): void
    {
        if ($this->getCurrentForUser($accountId) instanceof Config) {
            array_walk($this->storage, function (&$current) use ($accountId, $config) {
                if ($current['accountId'] === $accountId) {
                    $current['config'] = $config;
                }
            });

            return;
        }

        $this->storage[] = [
            'accountId' => $accountId,
            'config' => $config,
        ];
    }

    public function getCurrentForUser(string $accountId): ?Config
    {
        $filtered = array_filter($this->storage, function ($value) use ($accountId) {
            return $value['userIdent'] === $accountId;
        });

        $current = end($filtered);

        return $current['config'] instanceof Config ? $current['config'] : null;
    }

    /**
     * @inheritDoc
     */
    public function retrieveAll(): array
    {
        $output = [];

        foreach ($this->storage as $item) {
            $output[$item['accountId']] = $item['config'];
        }

        return array_filter($output);
    }
}
