<?php

declare(strict_types=1);

namespace TempoBot\Config\Exception;

use InvalidArgumentException;

class InputInvalidException extends InvalidArgumentException implements MessageIsUserReadable
{
}
