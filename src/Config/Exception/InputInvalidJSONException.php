<?php

declare(strict_types=1);

namespace TempoBot\Config\Exception;

use InvalidArgumentException;

class InputInvalidJSONException extends InvalidArgumentException
{
}
