<?php

declare(strict_types=1);

namespace TempoBot\Config\Exception;

interface MessageIsUserReadable
{
}
