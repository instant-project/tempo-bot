<?php

declare(strict_types=1);

namespace TempoBot\Config\Distribution;

use TempoBot\Config\Exception\InputInvalidException;
use TempoBot\Tempo\Worklog\Worklog;

class IssueKeyRegexMatcher implements BucketMatcher
{
    /** @var string */
    private $pattern;

    public function __construct($pattern)
    {
        if (!is_string($pattern) || strlen(trim($pattern)) <= 0) {
            throw new InputInvalidException("Pattern for matcher is empty.");
        }

        $this->pattern = $pattern;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function matchesWorklog(Worklog $worklog): bool
    {
        return (bool) @preg_match("/{$this->pattern}/", $worklog->getIssueKey());
    }

    public function jsonSerialize()
    {
        return $this->pattern;
    }
}
