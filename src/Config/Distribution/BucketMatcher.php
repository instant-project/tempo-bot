<?php

declare(strict_types=1);

namespace TempoBot\Config\Distribution;

use JsonSerializable;
use TempoBot\Tempo\Worklog\Worklog;

interface BucketMatcher extends JsonSerializable
{
    public function matchesWorklog(Worklog $worklog): bool;
}
