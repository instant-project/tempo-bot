<?php

declare(strict_types=1);

namespace TempoBot\Config;

use InvalidArgumentException;
use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidJSONException;

class Config extends BaseConfig
{
    /**
     * @param string $input
     *
     * @return Config
     * @throws InputInvalidJSONException
     * @throws InputEmptyException
     * @throws InvalidArgumentException
     */
    public static function fromJsonString(string $input): Config
    {
        return new Config($input);
    }
}