<?php

declare(strict_types=1);

namespace TempoBot\Cli\Commands;

use Carbon\CarbonImmutable;
use Predis\Client;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Repository\RedisConfigRepository;
use TempoBot\Tempo\Client\ClientFactory;
use TempoBot\Tempo\Services\CachedGetWorklogsService;
use TempoBot\Tempo\Services\GetWorklogsService;


$redisClient = new Client([
    'host' => $_SERVER['REDIS_HOSTNAME'],
]);
$configRepo = new RedisConfigRepository($redisClient, new ConfigFactory());


$baseDate = CarbonImmutable::now('UTC');
$startDate = $baseDate->subWeeks(2)->startOfDay();
$endDate = $baseDate->endOfDay();

$configs = $configRepo->retrieveAll();
foreach ($configs as $config) {
    $worklogsService = new GetWorklogsService(
        (new ClientFactory(
            $config->getTempoAccessToken()
        ))->build()
    );
    $cachedGetWorklogsService = new CachedGetWorklogsService($worklogsService, $redisClient);

    $cachedGetWorklogsService
        ->getWorklogs(
            $config->getTempoAccountId(),
            $startDate,
            $endDate
        );
}
