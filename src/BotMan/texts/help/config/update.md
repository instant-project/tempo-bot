`config update`

Use this command to change the configuration I use for building your reports. When you run it, I will first output your current configuration and then ask you to respond with the config you want.

If you have no config yet, I will provide you an empty configuration template. You can get started by just filling in your Tempo credentials.

If you respond with an invalid config I will tell what to fix and ask you again to respond with your desired configuration. I will not update your stored config until you respond with a valid one.

You can cancel the update by responding with `cancel`.
