`report`

I will respond with a report on the hours you have logged in Tempo:
- the total amount of hours logged in the current period (see Notes)
- the last time you logged hours into Tempo (to verify Tempo/I got your latest input)
- a list of days in the current period where you logged some hours, but less than 8 (in case you forgot to log all your hours)
- a distribution comparison table, where I compare the distribution of your logged hours over various projects to how you planned it (see Notes).


*Notes*
- "period" currently refers to the last two weeks (from 00:00 of the day 14 days ago up until the current moment).
- at the moment I have a timezone bug where hours worked after 22.00 Dutch time (GMT+2) are treated as being part of the next day

