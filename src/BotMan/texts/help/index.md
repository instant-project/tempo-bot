*QUICK START*
* read `help config update` and `help tempo setup`
* run `config update` and (only) input your Tempo credentials
* run `report`
* read `help config distribution` and start fine-tuning the report output


*Help subjects*
I can explain the following subjects (use `help {subject}`):
* config update
* tempo setup
* config distribution
* report
* about


*Commands*
I respond to the following commands:
* `help` this message
* `config update` set/update your personal configuration
* `report` report various things about your (un)logged Tempo hours
