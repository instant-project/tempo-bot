To set up our connection I need 2 things: an auth token and your accountId.

*AUTH TOKEN*
Go to Tempo (inside Jira) and then to Settings, API Integration.
Or click this link:
https://enrise.atlassian.net/plugins/servlet/ac/io.tempo.jira/tempo-configuration#!/api-integration
and then click API Integration.

Create a new Access token by clicking the "+ New Token" button, entering a name and 
clicking Confirm. Note that it might be convenient to choose a longer expiration period for the token 
than the default 30 days.

After you have created the token, copy it to your clipboard and add it to your config as the value 
of the "tempoAccessToken" key.



*ACCOUNT ID*
This is a bit easier:

* open this url: https://enrise.atlassian.net/rest/api/3/myself
* copy the value of the "accountId" key and add it to your config as the value of the "tempoAccountId" key.
