<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Repositories;

use Carbon\CarbonImmutable;
use Predis\Client;

class RedisWelcomeMessageRepository
{
    /** @var string */
    private const KEY_PREFIX = 'tempo-bot:repositories:welcome-message:%s';

    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'host' => $_SERVER['REDIS_HOSTNAME'],
        ]);
    }

    public function wasWelcomeMessageSend(string $accountId): bool
    {
        $value = $this->client->get($this->keyFor($accountId));

        if (!is_string($value)) {
            return false;
        }

        $decodedValue = json_decode($value, true);

        return is_array($decodedValue)
            && isset($decodedValue['messageSend'])
            && $decodedValue['messageSend'] === true;
    }

    protected function keyFor(string $accountId)
    {
        return sprintf(self::KEY_PREFIX, $accountId);
    }

    public function welcomeMessageWasSend(string $accountId): void
    {
        $this->client->set(
            $this->keyFor($accountId),
            json_encode([
                'messageSend' => true,
                'messageSendOn' => CarbonImmutable::now()->format(DATE_ATOM),
            ])
        );
    }
}
