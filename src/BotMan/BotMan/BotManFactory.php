<?php

declare(strict_types=1);

namespace TempoBot\BotMan\BotMan;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory as BotManClientFactory;
use BotMan\BotMan\Cache\RedisCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Slack\SlackDriver;
use Predis\Client;
use TempoBot\BotMan\Middleware\ConfigMiddleware;
use TempoBot\BotMan\Middleware\TempoAccessMiddleware;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Repository\RedisConfigRepository;

class BotManFactory
{
    public static function buildWithHardcodedConfig(): BotMan
    {
        $config = [
            'slack' => [
                'token' => $_SERVER['BOTMAN_SLACK_TOKEN'],
            ],
        ];
        $cache = new RedisCache($_SERVER['REDIS_HOSTNAME'], 6379);

        $client = new Client([
            'host' => $_SERVER['REDIS_HOSTNAME'],
        ]);

        DriverManager::loadDriver(SlackDriver::class);

        $botMan = BotManClientFactory::create($config, $cache);

        $botMan->middleware->received(new ConfigMiddleware(
            new RedisConfigRepository(
                $client,
                new ConfigFactory()
            ),
            new ConfigFactory()
        ));

        $botMan->middleware->received(new TempoAccessMiddleware());

        return $botMan;
    }
}
