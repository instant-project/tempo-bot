<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Commands;

use Carbon\CarbonImmutable;
use TempoBot\Config\Config;
use TempoBot\Config\Distribution\Bucket;
use TempoBot\Config\Distribution\BucketMatcher;
use TempoBot\Tempo\Services\GetWorklogsServiceInterface;
use TempoBot\Tempo\Worklog\Worklog;

final class ReportCommand implements CommandInterface
{
    /** @var GetWorklogsServiceInterface */
    private $worklogsService;

    public function __construct(GetWorklogsServiceInterface $worklogsService)
    {
        $this->worklogsService = $worklogsService;
    }

    public static function getCommandString(): string
    {
        return 'report';
    }

    public function handle(Config $config, CarbonImmutable $from, CarbonImmutable $till): string
    {
        $worklogs = $this->worklogsService->getWorklogs(
            $config->getTempoAccountId(),
            $from,
            $till,
        );

        $totalWorkedSeconds = $this->calculateTotalWorkedSeconds($worklogs);

        $mostRecent = $this->calculateMostRecentLogUpdate($from, $worklogs);

        $firstLine = sprintf(
            'You have worked %d hours since %s, and the last time you logged hours was %s.',
            ($totalWorkedSeconds / 3600),
            $from->format('Y-m-d'),
            $mostRecent->format('Y-m-d H:i')
        );

        $incompleteDays = $this->calculateIncompleteDays($worklogs);

        $distribution = $this->calculateDistribution($config->getDistribution(), $worklogs);

        return implode("\n\n", array_filter([
            $firstLine,
            $incompleteDays,
            $distribution,
        ]));
    }

    private function calculateTotalWorkedSeconds(array $worklogs): int
    {
        return array_reduce($worklogs, function (int $carry, Worklog $worklog) {
            return $carry + $worklog->getTimeSpentSeconds();
        }, 0);
    }

    private function calculateMostRecentLogUpdate(CarbonImmutable $from, array $worklogs): CarbonImmutable
    {
        $mostRecent = $from;
        /** @var Worklog $worklog */
        foreach ($worklogs as $worklog) {
            if ($worklog->getUpdated()->isAfter($mostRecent)) {
                $mostRecent = $worklog->getUpdated();
            }
        }

        return $mostRecent;
    }

    private function calculateIncompleteDays(array $worklogs): string
    {
        $output = '';

        $totalPerDay = [];
        /** @var Worklog $worklog */
        foreach ($worklogs as $worklog) {
            $index = $worklog->getStart()->format('Y-m-d');
            $totalPerDay[$index] = $totalPerDay[$index] ?? 0;
            $totalPerDay[$index] += $worklog->getTimeSpentSeconds();
        }

        $incompleteDays = array_filter($totalPerDay, function (int $secondsOnDay) {
            return $secondsOnDay < (8 * 60 * 60);
        });

        $indexOfToday = CarbonImmutable::now()->format('Y-m-d');
        unset($incompleteDays[$indexOfToday]);

        if (count($incompleteDays) > 0) {
            $output = "The following days have less than 8 hours logged, you might want to double check them:\n";

            array_walk($incompleteDays, function (int $value, string $key) use (&$output) {
                $output .= sprintf("%s (%s hours)\n", $key, round($value / 3600, 2));
            });
        }

        return $output;
    }

    /**
     * @param Bucket[]  $distribution
     * @param Worklog[] $worklogs
     *
     * @return string
     */
    private function calculateDistribution(array $distribution, array $worklogs): string
    {
        $desiredProps = $this->buildDesiredProps($distribution);
        $desiredProps = $this->addTotalToDesiredProps($desiredProps, $distribution, $worklogs);

        foreach ($worklogs as $worklog) {
            foreach ($desiredProps as &$desiredProp) {
                /** @var BucketMatcher $matchOn */
                foreach ($desiredProp['matchOn'] as $matchOn) {
                    if ($matchOn->matchesWorklog($worklog)) {
                        $desiredProp['workedSeconds'] += $worklog->getTimeSpentSeconds();
                        break 2;
                    }
                }
            }
        }

        array_walk($desiredProps, function (&$desiredProp) {
            $desiredProp['workedHours'] = round($desiredProp['workedSeconds'] / 3600, 2);
        });

        $distributionSummary = array_reduce($desiredProps, function (string $carry, $desiredProp) {
            $diff = $desiredProp['workedHours'] - $desiredProp['plannedHours'];
            return $carry . sprintf(
                    "%-'.20s %- 5s / %- 3s (%- 6s)\n",
                    $desiredProp['label'],
                    $desiredProp['workedHours'],
                    $desiredProp['plannedHours'],
                    ($diff == 0 ? ' 0' : ($diff < 0 ? $diff : "+$diff"))
                );
        }, '');

        return "Distribution:```\n" . $distributionSummary . "```\n";
    }

    /**
     * @param Bucket[] $distribution
     *
     * @return array
     */
    private function buildDesiredProps(array $distribution): array
    {
        $desiredProps = [];

        foreach ($distribution as $bucket) {
            $desiredProps[] = [
                'label' => $bucket->getLabel(),
                'matchOn' => $bucket->getMatchers(),
                'plannedHours' => $bucket->getPlannedHours(),
                'workedSeconds' => 0,
                'workedHours' => 0,
            ];
        }

        return $desiredProps;
    }

    /**
     * @param array $desiredProps
     * @param array $distribution
     * @param array $worklogs
     *
     * @return array
     */
    private function addTotalToDesiredProps(array $desiredProps, array $distribution, array $worklogs): array
    {
        array_push(
            $desiredProps,
            [
                'label' => 'Total',
                'matchOn' => [
                ],
                'plannedHours' => $this->calculateTotalPlannedHours($distribution),
                'workedSeconds' => $this->calculateTotalWorkedSeconds($worklogs),
                'workedHours' => 0,
            ]
        );

        return $desiredProps;
    }

    /**
     * @param Bucket[] $distribution
     *
     * @return int
     */
    private function calculateTotalPlannedHours(array $distribution): int
    {
        return array_reduce($distribution, function (int $carry, Bucket $bucket) {
            return $carry + $bucket->getPlannedHours();
        }, 0);
    }
}
