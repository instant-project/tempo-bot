<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Commands;

use BotMan\BotMan\BotMan;
use TempoBot\BotMan\Repositories\RedisWelcomeMessageRepository;

class WelcomeMessageCommand implements CommandInterface
{
    /** @var RedisWelcomeMessageRepository */
    private $repo;

    public function __construct(RedisWelcomeMessageRepository $repo)
    {
        $this->repo = $repo;
    }

    public static function getCommandString(): string
    {
        return 'app_home_opened';
    }

    public function handle(BotMan $bot)
    {
        $accountId = $bot->getUser()->getId();

        if ($this->shouldSendWelcomeMessage($accountId)) {
            $this->sendWelcomeMessage($bot);
            $this->logThatMessageWasSend($accountId);
        }
    }

    protected function shouldSendWelcomeMessage(string $accountId): bool
    {
        return !$this->repo->wasWelcomeMessageSend($accountId);
    }

    protected function sendWelcomeMessage(BotMan $bot): void
    {
        $bot->reply($this->getText('welcome'));
    }

    private function getText(string $filename): string
    {
        $path = sprintf('%s/BotMan/texts/%s.md', $_SERVER['project_root'], $filename);

        if (is_readable($path)) {
            return file_get_contents($path);
        }

        return '';
    }

    protected function logThatMessageWasSend(string $accountId): void
    {
        $this->repo->welcomeMessageWasSend($accountId);
    }
}
