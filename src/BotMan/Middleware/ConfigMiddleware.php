<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Received;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use TempoBot\BotMan\Commands\ReportCommand;
use TempoBot\Config\Config;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Repository\ConfigRepositoryInterface;

class ConfigMiddleware implements Received
{
    /** @var ConfigRepositoryInterface */
    private $configRepository;

    /** @var ConfigFactory */
    private $configFactory;

    public function __construct(ConfigRepositoryInterface $configRepository, ConfigFactory $configFactory)
    {
        $this->configRepository = $configRepository;
        $this->configFactory = $configFactory;
    }

    public function received(IncomingMessage $message, $next, BotMan $bot)
    {
        $config = $this->configRepository->getCurrentForUser($message->getSender());

        if ($config instanceof Config) {
            $message->addExtras(Config::class, $config);

        } else {
            $message->addExtras(Config::class, $this->configFactory->buildEmpty());

            if ($this->validConfigIsNeeded($message)) {
                $message->setText('help config invalid');
            }
        }

        return $next($message);
    }

    private function validConfigIsNeeded(IncomingMessage $message)
    {
        return in_array($message->getText(), [
            ReportCommand::getCommandString(),
        ]);
    }
}
