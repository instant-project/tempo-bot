<?php

declare(strict_types=1);

namespace TempoBot\EnvVars;

use Dotenv\Dotenv;
use RuntimeException;

class EnvVars
{
    public static function init()
    {
        self::guardAgainstMissingEnvFile();

        $dotenv = Dotenv::createImmutable(__DIR__);
        $dotenv->load();

        $_SERVER['project_root'] = dirname(__DIR__);
    }

    protected static function guardAgainstMissingEnvFile(): void
    {
        if (!is_readable(__DIR__ . '/.env')) {
            throw new RuntimeException('No .env file found at ' . __DIR__);
        }
    }
}
