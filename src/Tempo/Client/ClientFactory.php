<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Client;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use TempoBot\EnvVars\EnvVars;

class ClientFactory
{
    /** @var string */
    private $token;

    /** @var array */
    private $guzzleParams;

    public function __construct(string $token)
    {
        $this->token = $token;;

        $this->guzzleParams = [
            'base_uri' => 'https://api.tempo.io/core/3/',
            'timeout' => 30.0,
        ];
    }

    public static function withTokenFromEnv(): ClientFactory
    {
        return new self(
            $_SERVER['TEMPO_TOKEN']
        );
    }

    public function withNullHandler(): ClientFactory
    {
        $this->guzzleParams['handler'] = function () {
            return new Response();
        };
        return $this;
    }

    public function withResponseBodiesFromTheseFilesHandler(array $bodyFiles): ClientFactory
    {
        EnvVars::init();

        $responseBodies = array_map(function ($bodyFile) {
            $path = sprintf(
                '%s/../tests/unit/Tempo/Client/responseBodies/%s',
                $_SERVER['project_root'],
                $bodyFile
            );

            return file_get_contents($path);
        }, $bodyFiles);

        return $this->withTheseResponseBodiesHandler($responseBodies);
    }

    public function withTheseResponseBodiesHandler(array $responseBodies): ClientFactory
    {
        $responses = array_map(function ($responseBody) {
            return new Response(200, [], $responseBody);
        }, $responseBodies);

        $this->guzzleParams['handler'] = new MockHandler($responses);

        return $this;
    }

    public function withHandler(callable $handler): ClientFactory
    {
        $this->guzzleParams['handler'] = $handler;
        return $this;
    }

    public function build(): Client
    {
        $guzzleClient = new GuzzleClient($this->guzzleParams);

        return new Client($guzzleClient, $this->token);
    }
}
