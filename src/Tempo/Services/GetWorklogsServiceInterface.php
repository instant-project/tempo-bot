<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Services;

use Carbon\CarbonImmutable;
use TempoBot\Tempo\Worklog\Worklog;

interface GetWorklogsServiceInterface
{
    /**
     * @param string          $accountId
     * @param CarbonImmutable $from
     * @param CarbonImmutable $to
     *
     * @return Worklog[]
     */
    public function getWorklogs(string $accountId, CarbonImmutable $from, CarbonImmutable $to): array;
}
