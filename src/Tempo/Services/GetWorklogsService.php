<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Services;

use Carbon\CarbonImmutable;
use JsonException;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use TempoBot\Tempo\Client\Client;
use TempoBot\Tempo\Worklog\Worklog;

final class GetWorklogsService implements GetWorklogsServiceInterface
{
    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string          $accountId
     * @param CarbonImmutable $from
     * @param CarbonImmutable $to
     *
     * @return Worklog[]
     */
    public function getWorklogs(
        string $accountId,
        CarbonImmutable $from,
        CarbonImmutable $to
    ): array
    {
        $decodedWorklogs = [];
        $offset = 0;
        $decoded = $this->getDecodedResponse($accountId, $from, $to, $offset);
        $decodedWorklogs = array_merge($decodedWorklogs, $decoded->results);

        while ($this->hasMore($decoded)) {
            $offset += 50;
            $decoded = $this->getDecodedResponse($accountId, $from, $to, $offset);
            $decodedWorklogs = array_merge($decodedWorklogs, $decoded->results);
        }

        return $this->convertToWorklogArray($decodedWorklogs);
    }

    private function getDecodedResponse(
        string $clientId,
        CarbonImmutable $from,
        CarbonImmutable $to,
        int $offset
    ): stdClass
    {
        $response = $this->getResponse($clientId, $from, $to, $offset);

        try {
            return json_decode(
                $response->getBody()->getContents(),
                false,
                512,
                JSON_THROW_ON_ERROR
            );
        } catch (JsonException $e) {
            return new stdClass();
        }
    }

    private function getResponse(
        string $clientId,
        CarbonImmutable $from,
        CarbonImmutable $to,
        int $offset
    ): ResponseInterface
    {
        return $this->client->request("worklogs/user/{$clientId}", [
            'from' => $from->toDateString(),
            'to' => $to->toDateString(),
            'offset' => $offset,
        ]);
    }

    private function hasMore($decoded): bool
    {
        return is_object($decoded)
            && isset($decoded->metadata)
            && isset($decoded->metadata->next)
            && is_string($decoded->metadata->next)
            && $decoded->metadata->next
            && trim($decoded->metadata->next);
    }

    /**
     * @param array $decodedWorklogs
     *
     * @return Worklog[]
     */
    private function convertToWorklogArray(array $decodedWorklogs)
    {
        return array_map(function ($decodedWorklog) {
            return Worklog::fromApiOutput($decodedWorklog);
        }, $decodedWorklogs);
    }
}
