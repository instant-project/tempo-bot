# ======================
## Structural overview
#
## Targets
# builder: build environment from where all other project tasks
#   (running composer/tests, building images) can be run.
# base: environment in which the project can run (roughly equal to prod env.)
# others: see below
#
## base-composer-app targets
# For both dev and prod each, 3 targets are defined:
# base: environment to run the application in, but not the application or
#   its application-level dependencies themselves. Fully defined in this file,
#   so the image only has to be (re-) build when this changes. This speeds
#   up the tests part of the pipeline.
#
# composer: based on the related 'base' target, this also contains
#   composer, the application and application-level dependencies.
#
# app: also based on the related 'base' target, this contains just
#   the application and application-level dependencies
#
# ======================




# ======================
# builder target
# ======================
FROM alpine:3.10 AS builder

ENV LANG='en_US.UTF-8' LANGUAGE='en_US.UTF-8' TERM='xterm'

RUN apk -U --no-cache add \
    autoconf \
    automake \
    bash \
    curl \
    docker \
    git \
    grep \
    make \
    openssh

COPY --from=docker/compose:alpine-1.25.5 \
    /usr/local/bin/docker-compose /usr/local/bin/docker-compose

WORKDIR /etc/project



# ======================
# base target
# ======================
FROM existenz/webstack:7.3 AS base

ENV LANG='en_US.UTF-8' LANGUAGE='en_US.UTF-8' TERM='xterm'

RUN apk -U --no-cache add \
    php7 \
    php7-curl \
    php7-iconv \
    php7-intl \
    php7-json \
    php7-mbstring \
    php7-pecl-redis \
    php7-tokenizer

COPY ./ops/docker/app/files /

WORKDIR /etc/project




# ======================
# dev_base target
# ======================
FROM base AS dev_base

ARG USERID=1000
ARG GROUPID=1000

RUN apk -U --no-cache add \
    php7-dom \
    php7-openssl \
    php7-pecl-xdebug \
    php7-phar \
    php7-xml \
    php7-xmlwriter \
    shadow \
    && rm -rf /var/cache/apk/*

COPY ./dev/xdebug.ini /etc/php7/conf.d/xdebug.ini

RUN curl --silent --show-error https://getcomposer.org/installer | \
    php -- --install-dir=/usr/local/bin --filename=composer


# ======================
# dev_composer target
#
# intentionally combined with dev_app target, because there is no real advantage
# to having a dev_app container without composer and this is easier to follow
# ======================


# ======================
# dev_app target
# ======================
FROM dev_base AS dev_app

COPY . /etc/project
RUN ln -s /etc/project/src /www/public

RUN COMPOSER="/etc/project/conf/composer.json" composer install \
    --working-dir /etc/project/src \
    --prefer-dist \
    --no-progress \
    --no-suggest \
    --no-interaction \
    --no-scripts

ENTRYPOINT ["/init"]
CMD []



# ======================
# prod_base target
#
# only added for consistency with base-composer-app pattern
# ======================
FROM base AS prod_base


# ======================
# prod_composer target
# ======================
FROM prod_base AS prod_composer

RUN apk -U --no-cache add \
    php7-openssl \
    php7-phar

RUN curl --silent --show-error https://getcomposer.org/installer | \
    php -- --install-dir=/usr/local/bin --filename=composer

COPY . /etc/project
RUN ln -s /etc/project/src /www/public

RUN COMPOSER="/etc/project/conf/composer.json" composer install \
    --working-dir /etc/project/src \
    --no-dev \
    --prefer-dist \
    --no-progress \
    --no-suggest \
    --no-interaction \
    --no-scripts


# ======================
# prod_app target
# ======================
FROM prod_base AS prod_app

# Add the application to the container
COPY --from=prod_composer --chown=php:nginx /etc/project/src /www/public

ENTRYPOINT ["/init"]
CMD []
