<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TempoBot\Config\Config;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\EmptyConfig;
use TempoBot\Config\Repository\ArrayConfigRepository;
use TempoBot\Config\Repository\ConfigRepositoryInterface;

class ConfigMiddlewareTest extends TestCase
{
    /** @var ConfigMiddleware */
    private $subject;

    /** @var MockObject|ConfigFactory */
    private $configFactory;

    /** @var MockObject|ConfigRepositoryInterface */
    private $configRepository;

    /** @var MockObject|IncomingMessage */
    private $incomingMessage;

    public function testIfConfigIsAvailableItIsReturned()
    {
        $config = $this->createMock(Config::class);

        $this->configRepositoryExpectsGetCurrentForUser()
            ->willReturn($config);

        $this->assertSubjectSetsThisAsExtraOnMessage($config);
    }

    private function configRepositoryExpectsGetCurrentForUser(): InvocationMocker
    {
        return $this->configRepository
            ->expects(self::once())
            ->method('getCurrentForUser');
    }

    private function assertSubjectSetsThisAsExtraOnMessage($setExtraValue)
    {
        $bot = $this->createMock(BotMan::class);
        $this->subject->received($this->incomingMessage, function (IncomingMessage $message) use ($setExtraValue) {
            self::assertSame($setExtraValue, $message->getExtras(Config::class));
        }, $bot);
    }

    public function testIfConfigIsMissingEmptyConfigIsReturned()
    {
        $emptyConfig = new EmptyConfig();

        $this->configRepositoryExpectsGetCurrentForUser()
            ->willReturn(null);

        $this->configFactory
            ->expects(self::once())
            ->method('buildEmpty')
            ->willReturn($emptyConfig);

        $this->assertSubjectSetsThisAsExtraOnMessage($emptyConfig);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->configFactory = $this->createPartialMock(
            ConfigFactory::class,
            ['buildEmpty',]
        );
        $this->configRepository = $this->createPartialMock(
            ArrayConfigRepository::class,
            ['getCurrentForUser',]
        );
        $this->subject = new ConfigMiddleware(
            $this->configRepository,
            $this->configFactory
        );

        $this->incomingMessage = $this->createPartialMock(IncomingMessage::class, ['getSender']);
        $this->incomingMessage->method('getSender')->willReturn('some_sender');
    }
}
