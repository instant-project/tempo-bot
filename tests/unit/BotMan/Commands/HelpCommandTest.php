<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Commands;

use PHPUnit\Framework\TestCase;

class HelpCommandTest extends TestCase
{
    /** @var HelpCommand */
    private $subject;

    public function testHandleWithoutSubjectReturnsString()
    {
        $helpText = $this->subject->handle([]);

        self::assertIsString($helpText);
        self::assertNotEmpty($helpText);
    }

    public function testHandleWithSubjectReturnsString()
    {
        $helpText = $this->subject->handle(['subject' => 'stuff']);

        self::assertIsString($helpText);
        self::assertNotEmpty($helpText);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = new HelpCommand();
    }
}
