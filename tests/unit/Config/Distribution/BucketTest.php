<?php

declare(strict_types=1);

namespace TempoBot\Config\Distribution;

use PHPUnit\Framework\TestCase;
use stdClass;
use TempoBot\Config\Exception\InputInvalidException;

class BucketTest extends TestCase
{
    public function testSimpleValidInput()
    {
        $bucket = new Bucket(
            'label',
            ['.*',],
            11,
            14
        );
        self::assertInstanceOf(Bucket::class, $bucket);
    }

    /**
     * @dataProvider invalidInputProvider
     *
     * @param $label
     * @param $matchers
     * @param $plannedHours
     * @param $workedSeconds
     */
    public function testInvalidInputIsRejected($label, $matchers, $plannedHours, $workedSeconds)
    {
        $this->expectException(InputInvalidException::class);

        new Bucket($label, $matchers, $plannedHours, $workedSeconds);
    }

    public function invalidInputProvider()
    {
        return [
            'empty label' => [
                'label' => '',
                'matchers' => ['.*',],
                'plannedHours' => 0,
                'workedSeconds' => 0,
            ],
            'null matcher' => [
                'label' => 'label',
                'matchers' => [null,],
                'plannedHours' => 0,
                'workedSeconds' => 0,
            ],
            'wrong class matcher' => [
                'label' => 'label',
                'matchers' => [new stdClass()],
                'plannedHours' => 0,
                'workedSeconds' => 0,
            ],
            'negative planned hours' => [
                'label' => 'label',
                'matchers' => [new IssueKeyRegexMatcher('.*'),],
                'plannedHours' => -1,
                'workedSeconds' => 0,
            ],
            'negative worked seconds' => [
                'label' => 'label',
                'matchers' => [new IssueKeyRegexMatcher('.*'),],
                'plannedHours' => 0,
                'workedSeconds' => -1,
            ],
        ];
    }

    public function testChangingWorkedSecondsChangesWorkedHours()
    {
        $oneHour = 60 * 60;
        $bucket = new Bucket('label', ['.*',], 0, $oneHour);

        $initialWorkedHours = $bucket->getWorkedHours();
        $bucket->setWorkedSeconds(2 * $oneHour);

        self::assertSame(2 * $oneHour, $bucket->getWorkedSeconds());
        self::assertTrue($initialWorkedHours != $bucket->getWorkedHours());
    }

    public function testMostFieldsAreIncludedWhenSerializingToJson()
    {
        $label = 'label';
        $matchers = ['.*',];
        $plannedHours = 14;
        $bucket = new Bucket($label, $matchers, $plannedHours);

        $serializable = $bucket->jsonSerialize();

        self::assertSame($label, $serializable->label);
        self::assertSame(count($matchers), count($serializable->matchers));
        self::assertSame($matchers[0], $serializable->matchers[0]);
        self::assertSame($plannedHours, $serializable->plannedHours);
    }

    public function testWorkedSecondsAreNotSerializedToJson()
    {
        $bucket = new Bucket('label', ['.*',], 0, 14);

        $serializable = $bucket->jsonSerialize();

        self::assertTrue(!isset($serializable->workedSeconds));
    }

    public function testItRequiresAtLeastOneMatcher()
    {
        $bucket = new Bucket('label', ['.*',]);
        self::assertInstanceOf(Bucket::class, $bucket);

        $this->expectException(InputInvalidException::class);
        new Bucket('label', []);
    }
}
