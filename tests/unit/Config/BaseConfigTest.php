<?php

declare(strict_types=1);

namespace TempoBot\Config;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;
use TempoBot\Config\Distribution\Bucket;
use TempoBot\Config\Exception\InputInvalidException;

class BaseConfigTest extends TestCase
{
    /** @var BaseConfig */
    private $subject;

    public function testEmptyJsonOutput()
    {
        $prettyJson = <<<'PRETTY_JSON'
{
    "tempoAccountId": "",
    "tempoAccessToken": "",
    "distribution": [
        {
            "label": "bucketLabel",
            "matchers": [
                ".*"
            ],
            "plannedHours": 1
        }
    ]
}
PRETTY_JSON;

        self::assertSame(
            $prettyJson,
            $this->subject->asPrettyJson()
        );
    }

    public function testPropertyTempoAccountId()
    {
        $initialAccountId = $this->subject->getTempoAccountId();
        $this->subject->setTempoAccountId('new tempo account id');
        self::assertTrue(
            $initialAccountId !== $this->subject->getTempoAccountId(),
            'setting account id should update it'
        );

        $this->expectException(InvalidArgumentException::class);
        $this->subject->setTempoAccountId('');
    }

    public function testPropertyTempoAccessToken()
    {
        $initialAccountId = $this->subject->getTempoAccessToken();
        $this->subject->setTempoAccessToken('new tempo access token');
        self::assertTrue(
            $initialAccountId !== $this->subject->getTempoAccessToken(),
            'setting tempo access token should update it'
        );

        $this->expectException(InvalidArgumentException::class);
        $this->subject->setTempoAccessToken('');
    }

    public function testSettingDistributionShouldUpdateIt()
    {
        $initialDistribution = $this->subject->getDistribution();
        $this->subject->setDistribution([
            $this->createMock(Bucket::class)
        ]);

        self::assertTrue(
            $initialDistribution != $this->subject->getDistribution(),
            'setting distribution should update it'
        );
    }

    public function testDistributionCannotBeSetToEmpty()
    {
        $this->expectException(InputInvalidException::class);
        $this->subject->setDistribution([]);
    }

    public function testDistributionCanOnlyContainBuckets()
    {
        $this->subject->setDistribution([
            $this->createMock(Bucket::class)
        ]);

        $this->expectException(InputInvalidException::class);
        $this->subject->setDistribution([
            new stdClass()
        ]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = new EmptyConfig();
    }
}