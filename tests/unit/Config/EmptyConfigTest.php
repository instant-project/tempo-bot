<?php

declare(strict_types=1);

namespace TempoBot\Config;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class EmptyConfigTest extends TestCase
{
    /** @var EmptyConfig */
    private $subject;

    public function testConstructingWithoutParametersWorks()
    {
        self::assertInstanceOf(EmptyConfig::class, $this->subject);
    }

    public function testSubjectHasEmptyProperties()
    {
        self::assertEmpty($this->subject->getTempoAccountId());
        self::assertEmpty($this->subject->getTempoAccessToken());
    }

    public function testSettingPropertiesIsStillPossible()
    {
        $tempoAccessToken = 'test_token';
        $this->subject->setTempoAccessToken($tempoAccessToken);

        self::assertSame($tempoAccessToken, $this->subject->getTempoAccessToken());
    }

    public function testSettingPropertiesToEmptyValuesIsNotPossible()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->subject->setTempoAccessToken('');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = new EmptyConfig();
    }
}
