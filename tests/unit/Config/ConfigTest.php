<?php

declare(strict_types=1);

namespace TempoBot\Config;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidJSONException;

class ConfigTest extends TestCase
{
    public function testEmptyStringThrowsException()
    {
        $this->expectException(InputEmptyException::class);
        Config::fromJsonString('');
    }

    public function testInvalidJSONThrowsException()
    {
        $this->expectException(InputInvalidJSONException::class);
        Config::fromJsonString('}');
    }

    public function testConstructionWithEmptyPropertiesThrowsException()
    {
        $this->expectException(InvalidArgumentException::class);
        Config::fromJsonString('{
            "tempoAccountId": "",
            "tempoAccessToken": ""
        }');
    }

    public function testConstructionWithAccountIdAndAccessTokenSucceeds()
    {
        $config = Config::fromJsonString('{
            "tempoAccountId": "foo",
            "tempoAccessToken": "bar",
            "distribution": [
                {
                    "label": "label",
                    "matchers": [
                        ".*"
                    ],
                    "plannedHours": 0
                }
            ]
        }');

        self::assertInstanceOf(Config::class, $config);
    }

    public function testConstructorShouldNotCrashOnIncompleteInput()
    {
        $this->expectException(InvalidArgumentException::class);
        // the same JSON as with testConstructionWithAccountIdAndAccessTokenSucceeds
        // but with the "tempoAccountId" line removed
        Config::fromJsonString('{
            "tempoAccessToken": "bar",
            "distribution": [
                {
                    "label": "label",
                    "matchers": [
                        ".*"
                    ],
                    "plannedHours": 0
                }
            ]
        }');
    }

    public function testJsonDeSerialisationCanFeedItself()
    {
        $prettyJson = <<<'PRETTY_JSON'
{
    "tempoAccountId": "foo",
    "tempoAccessToken": "bar",
    "distribution": [
        {
            "label": "label",
            "matchers": [
                 ".*"
            ],
            "plannedHours": 0
        }
    ]
}
PRETTY_JSON;

        $subject = Config::fromJsonString($prettyJson);
        $sonOfSubject = Config::fromJsonString($subject->asPrettyJson());

        self::assertJsonStringEqualsJsonString($prettyJson, $sonOfSubject->asPrettyJson());
    }
}
