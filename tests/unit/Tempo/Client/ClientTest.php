<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Client;

use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    private $subject;

    /**
     * @group semiFunctionalTests
     */
    public function testCanConnectToTempo(): void
    {
        self::markTestSkipped('Skip until I move this to feature tests suite (so CI no longer runs it)');
        $this->subject = ($this->getClientFactory())->build();
        $response = $this->subject->canConnect();
        self::assertNotEmpty($response);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = ($this->getClientFactory())
            ->withNullHandler()
            ->build();
    }

    private function getClientFactory(): ClientFactory
    {
        return ClientFactory::withTokenFromEnv();
    }
}
