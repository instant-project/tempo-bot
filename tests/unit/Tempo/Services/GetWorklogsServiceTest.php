<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Services;

use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use TempoBot\Tempo\Client\ClientFactory;
use TempoBot\Tempo\Worklog\Worklog;

class GetWorklogsServiceTest extends TestCase
{
    /** @var GetWorklogsService */
    private $subject;

    public function testGetWorklogsReturnsWorklogs()
    {
        $client = (ClientFactory::withTokenFromEnv())
            ->withResponseBodiesFromTheseFilesHandler(['my_worklogs_of_past_two_weeks.json',])
            ->build();

        $this->subject = new GetWorklogsService($client);

        $worklogs = $this->subject->getWorklogs(
            'does_not_matter',
            CarbonImmutable::now()->subWeek(),
            CarbonImmutable::now(),
        );

        self::assertIsArray($worklogs);
        foreach ($worklogs as $worklog) {
            self::assertInstanceOf(Worklog::class, $worklog);
        }
    }

    protected function setUp(): void
    {
        parent::setUp();

        $client = (ClientFactory::withTokenFromEnv())
            ->build();

        $this->subject = new GetWorklogsService($client);
    }
}
